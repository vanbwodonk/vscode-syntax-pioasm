# PIO assembly syntax highlighting

PIO ASM extension add syntax highlighting to .pio files written using Raspberry Pi Pico PIO assembly language. 
## Features

By using this extension you will get colored directives, labels, instructions, arguments and comments.

![Picoprobe after syntax highlighting](images/image1.png)

I found it much easier to work with pio asm using syntax highlighting, hope you will get the same experience :)
## Release Notes
### 1.0.0

Initial release

-----------------------------------------------------------------------------------------------------------

**Enjoy!**
